import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import {NavigationContainer} from '@react-navigation/native';
import Router from './src/router/index'
import { Provider } from 'react-redux';
import { store } from './src/redux/store';
// import { NavigationContainer } from '@react-navigation/native';
// import NetInfo from '@react-native-community/netinfo';

const App = () => {
  return (

    <Provider store={store}>
    <NavigationContainer>
      <Router />
      </NavigationContainer>
    </Provider>
  
  )
}

export default App

const styles = StyleSheet.create({})