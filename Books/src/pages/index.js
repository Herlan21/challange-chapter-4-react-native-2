import SplashScreen from './SplashScreen'
import RegisterScreen from './RegisterScreen'
import LoginScreen from './LoginScreen'
import SuccessScreen from './SuccessScreen'
import HomeScreen from './HomeScreen'
import DetailBook from './DetailScreen'

export {SplashScreen, RegisterScreen, LoginScreen, SuccessScreen, HomeScreen, DetailBook}