import { StyleSheet, Text, View, Touchableopacity, Image, ScrollView } from 'react-native';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { homeSuccess } from '../../redux/actions';
import { RecommendedBook, PopularBook } from '../../components';

const HomeScreen = () => {

  const dispatch = useDispatch()

  const bookData = useSelector(state => {
    console.log('databuku', state.appData.bookData);
    return state.appData.bookData
  })

  const isLoading = useSelector(state => {
    console.log('databuku', state.appData.loading);
    return state.appData.loading
  })

  const token = useSelector(state => {
    console.log('token', state.appData.userData.tokens.access.token);
    return state.appData.userData
  })


  useEffect(() => {
    dispatch(homeSuccess(token.tokens.access.token))
  }, [])

  if (!isLoading) {



    return (


      <ScrollView>
        <View>

          {/* //! RECOMENDED */}
          <Text style={styles.textRecommended}>Recommended</Text>

          <ScrollView horizontal={true}>
            <View>
              <RecommendedBook data={bookData} />
            </View>
          </ScrollView>

          {/*//! PoPularBook */}
          <Text style={styles.textRecommended}>Popular Book</Text>
          <ScrollView horizontal={true}>
            <PopularBook data={bookData} />
          </ScrollView>

        </View>
      </ScrollView>

    )
  } else {
    return <Text style={styles.textRecommended}>....... Loading ......</Text>
  }
}

export default HomeScreen

const styles = StyleSheet.create({

  textRecommended: {
    fontSize: 20,
    marginTop: 15,
    marginLeft: 25,
    fontWeight: 'bold',
    color: '#8591d6',
  }
})