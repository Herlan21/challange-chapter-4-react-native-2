import { StyleSheet, Text, View, Image, Dimensions } from 'react-native'
import React from 'react'
import { useEffect } from 'react';
import LottieView from 'lottie-react-native';

const SplashScreen = ({ navigation }) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('Register')
    }, 4000)
  }, [navigation])
 

  return (
    <View style={styles.bg}>
      <View style={styles.logo}>
        <LottieView source={require('../../assets/splash.json')} autoPlay loop />

        <View>
          <Image style={styles.namalogo} source={require('../../assets/img/Book.png')} />

          <View style={{ top: 200 }}>
          <Text style={styles.footer}>Herlan Nurachman</Text>
          </View>
        </View>
      </View>
    </View>
  );
};

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export default SplashScreen

const styles = StyleSheet.create({

  logo: {
    width: windowWidth,
    height: windowHeight,
  },

  namalogo: {
    alignSelf: 'center',
    width: windowWidth,
    
  },

  footer: {
    color: '#72808A',
    fontWeight: '700',
    fontFamily: 'sans-serif-condensed',
    textAlign: 'center'
  },

  bg: {
    backgroundColor: '#001920',
    height: windowHeight
  }
})