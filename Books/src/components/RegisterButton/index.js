import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'


const RegisterButton = ({ onPress }) => {

    return (
        <View style={styles.LoginButton}>
            <TouchableOpacity
                onPress={onPress}>
                <Text style={styles.register}>Sign Up</Text>
            </TouchableOpacity>
        </View>
    )
}

export default RegisterButton

const styles = StyleSheet.create({

    register: {
        color: '#FFF',
        backgroundColor: '#8591d6',
        textAlign: 'center',
        width: 100,
        height: 35,
        borderRadius: 8,
        fontSize: 21,
        fontWeight: 'bold'
    },

    LoginButton: {
        alignItems: 'center',
        top: 5
    },
})